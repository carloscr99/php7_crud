<?php
// Archivo php para el "administrador" para crear la base de datos necesaria
// y las tablas, con los campos requeridos.

$servidor="127.0.0.1"; // "localhost"
$usuario_bd="carlos"; // Usuario Administrador de MySQL
$clave_bd="1234"; // Clave del Usuario Administrador de MySQL
$basedatos="examen";
$tabla1="cotxes";

$sql_crearbasedatos = "CREATE DATABASE $basedatos";

$sql_creartabla1 = "CREATE TABLE $tabla1(";
$sql_creartabla1.= "id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, marca CHAR(30) NOT NULL, model CHAR(80) NOT NULL, foto TEXT NOT NULL);";

$sql_insertarregistros1 = "INSERT INTO $tabla1 VALUES ";

$sql_insertarregistros1.= "('null','Opel','Astra', 'https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Opel-Astra_2016_A01.jpg?itok=duHYZWW9'),";
$sql_insertarregistros1.= "('null','tesla','Model 3', 'https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Tesla-Model_3-2018-C01.jpg?itok=5nZQjWRU');";


// Inicialmente intentaremos conectar con el servidor MySQL instalado en el servidor web.
$conexion=mysqli_connect($servidor,$usuario_bd,$clave_bd);
if (! $conexion){
	echo "ERROR: Imposible establecer conexión con el servidor (puede que esté desactivado o que no se encuentre en el servidor $servidor).<br>\n";
}
else{
 	// Como pudo conectarse con el servidor, intentaremos crear la base de datos, y después la seleccionaremos para poder trabajar sobre ella.
 	echo "Conexion realizada con el servidor.<br>\n";

	// CREAR LA BASE DE DATOS:
	$resultado=mysqli_query($conexion, $sql_crearbasedatos);
	if (! $resultado) {
		echo "ERROR: Imposible crear base de datos $basedatos (puede que ya exista o que no se tenga permiso para crearla).<br>\n";
	}
	else{
	 	echo "Base de datos $basedatos creada.<br>\n";
	}

	// SELECCIONAR LA BASE DE DATOS:
	$resultado=mysqli_select_db($conexion, $basedatos);
	if (! $resultado){
		echo "ERROR: Imposible seleccionar la base de datos $basedatos (puede que no exista o que no se tenga permiso para usarla).<br>\n";
	}
	else{
	 	// Como pudo seleccionarse la base de datos, entonces intentaremos crear las tablas dentro, junto con registros iniciales para pruebas.
	 	echo "Base de datos $basedatos seleccionada.<br>\n";
		// CREAR TABLA 1:
		$resultado=mysqli_query($conexion, $sql_creartabla1);
		if (! $resultado) {
			echo "ERROR: Imposible crear la tabla $tabla1 (puede que ya exista o que no se tenga permiso para crearla).<br>\n";
		}
		else
		{
			echo "Tabla $tabla1 creada.<br>\n";
		}
		// INSERTAR REGISTROS EN TABLA 1:
		$resultado=mysqli_query($conexion, $sql_insertarregistros1);
		if (! $resultado) {
			echo "ERROR: Imposible insertar registros iniciales en tabla $tabla1 (puede que ya existan esos registros o que no se tengan los permisos).<br>\n";
		}
		else
		{
			echo "Registros iniciales insertados satisfactoriamente en la Tabla $tabla1.<br>\n";
		}



	}
	// Antes de terminar, debe cerrarse la conexión con el servidor (pues sigue abierta)).
	echo "Cerrando la conexion con el servidor...<br>\n";
	mysqli_close($conexion);
}

echo "Fin del programa.<br>\n";
?>
